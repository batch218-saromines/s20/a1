

let n = Number(prompt("Give me a number: "));
console.log("The number you provided is " + n + ".");

for (let i = n; i > 0; i--) {

  if(i <= 50){
    console.log("The current value is at 50. Terminating the loop.");
    break;

  }

  if (i % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number");
    continue;
  }
  if (i % 5 === 0) {
    console.log(i);
    continue;
  }

}



// --------------


let v = "supercalifragilisticexpialidocious";

let z = "";
for (let i = 0; i < v.length; i++) {
  let a = v[i].toLowerCase();

  if (a == "a" || 
      a == "e" || 
      a == "i" || 
      a == "o" || 
      a == "u") {

    continue;
  
  } else {
    z += v[i];
  }
}
console.log("supercalifragilisticexpialidocious");
console.log(z);
